// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyAgnYVIDyvIQrbeAFaC15tO8x9nDkDEXEU",
    authDomain: "hex6-5c13a.firebaseapp.com",
    databaseURL: "https://hex6-5c13a.firebaseio.com",
    projectId: "hex6-5c13a",
    storageBucket: "hex6-5c13a.appspot.com",
    messagingSenderId: "733241461440"
  }
 };
 
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
