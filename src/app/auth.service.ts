import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';







@Injectable({
  providedIn: 'root'
})

export class AuthService {

  login(email: string, password: string) {
    return this.fireBaseAuth.auth.signInWithEmailAndPassword(email, password);
  } 
  
signup(email:string,password:string){
  return this.fireBaseAuth
   .auth
   .createUserWithEmailAndPassword(email,password);
 }

 logOut() {
  return this.fireBaseAuth.auth.signOut();
}


 user: Observable<firebase.User>;
 updateProfile(user,name:string) {
  user.updateProfile({displayName: name, photoURL: ''});
}
addUser(user, name: string) {
  let uid = user.uid;
  let ref = this.db.database.ref('/'); // the main endpoint of the db
  ref.child('users').child(uid).push({'name':name}); // if the child doesn't exist - he creates it
}
isAuth()
  {
    return this.fireBaseAuth.authState.pipe(map(auth => auth));
  }


  constructor(private fireBaseAuth:AngularFireAuth,private db:AngularFireDatabase    ) { 
    this.user = fireBaseAuth.authState;

  }
}

