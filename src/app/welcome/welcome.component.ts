import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { TodosService } from '../todos.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';







@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  todos = [];
  text:string;
  sorting: string;
  sortodos:any[];

  ifStatus:boolean; //בדיקה מה הסטטוס שבוחרים בפילטר
      todoStatus:boolean; // סטטוס של טודו חדש שמכניסים
      key;
      taskStatus = [];
    
 
/*
  addTodo() {
    this.todosService.addTodo(this.text);
    this.text = '';
  }

*/

addTodo(){
          this.todosService.addTodo(this.text, this.todoStatus);
          this.text = '';
          this.todoStatus = false;
          
        }
        showTodo()
        {
          this.authService.user.subscribe(user => {
            this.db.list('/users/' + user.uid + '/todos').snapshotChanges().subscribe(
              todos => {
                this.todos = [];
                todos.forEach(
                  todo => {
                    let y = todo.payload.toJSON();
                    y["$key"] = todo.key;
                    this.todos.push(y);
                  }
                )
             }  
            )
          })
        }
  
    toFilter() {
          this.authService.user.subscribe(user => {
            this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(
              todos =>{
                this.todos = [];
                todos.forEach(
                  todo => {
                    let y = todo.payload.toJSON();
                    y["$key"] = todo.key;          
                      if (this.ifStatus == y['status']) {
                        this.todos.push(y);
                    }
                      else if (this.ifStatus != true && this.ifStatus != false)
                     {
                      this.todos.push(y);
                      }
                      
                  }
                )
              }
            )
             })
            }
   
  sort(){
    console.log('s')
   if(this.sorting == "1"){
   this.todos = this.todos.sort((a,b)=>b.done - a.done);}
   if(this.sorting == "2"){
    this.todos =  this.todos.sort((a,b)=>a.done - b.done);}
  }
 
  constructor(private db:AngularFireDatabase,
    public authService:AuthService,
    private todosService:TodosService,private router:Router) { this.showTodo(); }
    
    ngOnInit() {
      this.authService.user.subscribe(user => {
        this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(
          todos =>{
            this.todos = [];
            this.taskStatus = [];
            todos.forEach(
              todo => {
                let y = todo.payload.toJSON();
                y["$key"] = todo.key;
                this.todos.push(y);
                let sta = y['status'];
                if (this.taskStatus.indexOf(sta)== -1)
                 {
                 this.taskStatus.push(sta);
                }
               
              }
            )
          }
        ) 
      })
    }
    /*

    ngOnInit() {
      this.authService.user.subscribe(
        user => {
          this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe( // tell me for all the data changes on the things below
            todos => { // the array he took from the FireBase
              this.todos = [];
              todos.forEach(
                todo => {
                  let task = todo.payload.toJSON();
                  task['key'] = todo.key;
                  this.todos.push(task);
                }
              )
            }
          )
        }
      )
    }
   

 */
  
}
