import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class TodosService {

  
      addTodo(text:string,status:boolean){
        this.authService.user.subscribe(user =>{
          this.db.list('/users/'+user.uid+'/todos').push({'text':text, 'status':false});
         })
       }
      
         
    updateTodo(key:string, text:string, status:boolean){
         this.authService.user.subscribe(user =>{
          this.db.list('/users/'+user.uid+'/todos').update(key,{'text':text,'status':status});
          })
        }

        update(key, text) {
          this.authService.user.subscribe(
            user => {
              this.db.list('users/'+user.uid+'/todos').update(key,{'text':text});
            }
          )
        
          }
               
        
                 updateStatus(key:string, text:string, status:boolean)
               {
                 this.authService.user.subscribe(user =>{
                  this.db.list('/users/'+user.uid+'/todos').update(key,{'text':text, 'status':status});
                 })
                 
               }



/*
  addTodo(todo: string) {
    this.authService.user.subscribe(
      user => {
        this.db.list('/users/'+user.uid+'/todos').push({'text': todo});
      }
    )
  }*/



  deleteTodo(key: string) {
    this.authService.user.subscribe(
      user => {
        this.db.list('users/'+user.uid+'/todos').remove(key);
      }
    )
  }
 /*
  update(key, text) {
    this.authService.user.subscribe(
      user => {
        this.db.list('users/'+user.uid+'/todos').update(key,{'text':text});
      }
    )
  }
*/

 
  constructor(private authService:AuthService,
    private db:AngularFireDatabase,
  ) { }


  
}
