import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"
import { AuthService } from '../auth.service';



@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private router:Router,public authService:AuthService ) { }

  ngOnInit() {
  }
  toSignup() {
    this.router.navigate(['/'])
  }
 
  toLogin() {
    this.router.navigate(['/login'])
  }
 
  toWelcome() {
    this.router.navigate(['/welcome'])
  }

  logOut() {
    this.authService.logOut()
    .then(value => {
      this.router.navigate(['/login'])
    }).catch(err => {
      console.log(err)
    });
  }
 
 
  
}
